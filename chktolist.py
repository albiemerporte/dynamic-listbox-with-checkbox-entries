
from PyQt6.uic import loadUi
from PyQt6.QtWidgets import QApplication
from PyQt6.QtCore import Qt


class ChkToList:
    def __init__(self):
        self.mainui = loadUi('chktolist.ui')
        self.mainui.show()
        self.mainui.chk1.stateChanged.connect(self.check1)
        self.mainui.chk2.stateChanged.connect(self.check2)
        self.mainui.chk3.stateChanged.connect(self.check3)
        self.mainui.chk4.stateChanged.connect(self.check4)
        
    def chkproc(self, mychk, data):
        if mychk:
            self.mainui.listMain.addItem(data)
        else:
            items = self.mainui.listMain.findItems(data, Qt.MatchFlag(0))
            
            for item in items:
                row = self.mainui.listMain.row(item)
                self.mainui.listMain.takeItem(row)
    
    def check1(self):
        chked1 = self.mainui.chk1.isChecked()
        data = 'Apple'
        
        self.chkproc(chked1, data)
        
    def check2(self):
        chked2 = self.mainui.chk2.isChecked()
        data = 'Orange'
        
        self.chkproc(chked2, data)

    def check3(self):
        chked3 = self.mainui.chk3.isChecked()
        data = 'Mango'
        
        self.chkproc(chked3, data)

    def check4(self):
        chked4 = self.mainui.chk4.isChecked()
        data = 'Banana'
        
        self.chkproc(chked4, data)

if __name__ == '__main__':
    app = QApplication([])
    main = ChkToList()
    app.exec()